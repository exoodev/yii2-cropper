<?php

namespace exoo\cropper;

/**
 * Asset bundle for widget [[Cropper]].
 */
class CropperAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/cropperjs/dist';
    /**
     * @inheritdoc
     */
    public $css = [
        'cropper.min.css',
    ];
    /**
     * @inheritdoc
     */
    public $js = [
        'cropper.min.js',
    ];
}
